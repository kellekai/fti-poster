EDITOR := okular

.PHONY: view clean

fti_poster.pdf: fti_poster.tex Makefile
	pdflatex --enable-write18 $< &&	pdflatex $< && pdflatex $<

view: fti_poster.pdf
	$(EDITOR) fti_poster.pdf

clean:
	  rm -f *.aux	*.bbl	*.blg	*.log	poster.pdf 
